/* Copyright (C) 1996-2015 Piotr Held
 *
 * This file is part of Octave.
 *
 * Octave is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Octave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Octave; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 */

/* Author: Piotr Held <pjheld@gmail.com>. 
 * This function is based on ikeda of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/
#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include <complex>

DEFUN_DLD (__ikeda__, args, , HELPTEXT)
{
    int nargin = args.length ();
    octave_value_list retval;
    
    if (nargin != 7)
      {
        print_usage ();
      }
    else
      {

        octave_idx_type nmax  = args(0).idx_type_value();
        double a              = args(1).double_value();
        double b              = args(2).double_value();
        double c              = args(3).double_value();
        double x0             = args(4).double_value();
        double y0             = args(5).double_value();
        octave_idx_type ntran = args(6).idx_type_value();

        Matrix ret (nmax,2);

        for (octave_idx_type k = 0; k < ntran; k++)
          {
            double s=a-b/(1.+x0*x0+y0*y0);
            double cs=cos(s);
            double ss=sin(s);
            double xn=1.+c*(x0*cs-y0*ss);
            double yn=c*(x0*ss+y0*cs);
            x0=xn;
            y0=yn;
          }

        for (int k = 0; k < nmax; k++)
          {
            double s=a-b/(1.+x0*x0+y0*y0);
            double cs=cos(s);
            double ss=sin(s);
            double xn=1.+c*(x0*cs-y0*ss);
            double yn=c*(x0*ss+y0*cs);
            x0=xn;
            y0=yn;
            ret(k,0) = x0;
            ret(k,1) = y0;
          }
   
        retval(0) = ret;

      }
    
    return retval;
}
