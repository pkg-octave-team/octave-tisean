/* Copyright (C) 1996-2015 Piotr Held
 *
 * This file is part of Octave.
 *
 * Octave is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Octave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Octave; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 */

/* Author: Piotr Held <pjheld@gmail.com>. 
 * This function is based on spikeauto of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/
#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>

DEFUN_DLD (__spikeauto__, args, , HELPTEXT)
{
    int nargin = args.length ();
    octave_value_list retval;

    if (nargin != 3)
      {
        print_usage ();
      }
    else
      {
        // Assign input variables
        Matrix X       = args(0).matrix_value();
        double bin     = args(1).double_value();
        octave_idx_type nbin  = args(2).idx_type_value();

        // Extract pointer to X for optimization purposes
        double *X_ptr = X.fortran_vec ();

        // Prepare output ihist
        Array<octave_idx_type> ihist (dim_vector(nbin,1));
        ihist.fill(0);
        octave_idx_type *ihist_ptr = ihist.fortran_vec ();

        // Calculate output
        for (octave_idx_type n1 = 0; n1 < X.rows (); n1++)
          for (octave_idx_type n2 = n1 + 1; n2 < X.rows (); n2++)
            {
              octave_idx_type il;
              il = (octave_idx_type)((X_ptr[n2] - X_ptr[n1]) / bin);
              if (il < nbin)
                ihist_ptr[il] += 1;
            }

        retval(0) = ihist;
      }

    return retval;
}
