      FUNCTION r1MACH (I)
c     this is all for doubles so that everything works properly
c     as the tisean package must be always compiled with freal-4-real-8
c this is not the original one from slatec 
      double precision const(5)
c small:
      DATA const(1) / 2.23D-308  /
c large:
      DATA const(2) / 1.79D+308  /
c diff:
      DATA const(3) / 1.11D-16   /
      DATA const(4) / 2.22D-16   /
c log10:
      DATA const(5) / 0.301029995663981195D0 / 

C***FIRST EXECUTABLE STATEMENT  D1MACH
C
      R1MACH = const(I)
      RETURN
C
      END


