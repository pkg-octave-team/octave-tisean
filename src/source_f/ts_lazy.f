c===========================================================================
c
c   This file is part of TISEAN
c 
c   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
c                           Piotr Held
c   TISEAN is free software; you can redistribute it and/or modify
c   it under the terms of the GNU General Public License as published by
c   the Free Software Foundation; either version 2 of the License, or
c   (at your option) any later version.
c
c   TISEAN is distributed in the hope that it will be useful,
c   but WITHOUT ANY WARRANTY; without even the implied warranty of
c   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c   GNU General Public License for more details.
c
c   You should have received a copy of the GNU General Public License
c   along with TISEAN; if not, write to the Free Software
c   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
c
c===========================================================================
c   ts_lazy.f
c   simple nonlinear noise reduction
c   see  H. Kantz, T. Schreiber, Nonlinear Time Series Analysis, Cambridge
c      University Press (1997,2004)
c   author T. Schreiber (1998)
c===========================================================================
c   Modified: Piotr Held <pjheld@gmail.com> (2015). 
c   This function is based on lazy.f of TISEAN 3.0.1 https://github.com/heggus/Tisean"
c===========================================================================

      subroutine ts_lazy(m, rv, imax, lines_read,
     $                   in_out1, in_out2)
c      implicit none

c     -- input variables --
      integer*4 m
      real*8 rv
      integer*4 imax, lines_read

c     -- input arrays --
      real*8 in_out1(lines_read), in_out2(lines_read)

c     -- Assigning type for old TISEAN variables --
      real*8 eps, frac, sc, sd
      integer*4 n, it, nmax
      integer*4 iverb
      parameter (iverb=1)
      real*8 x0(lines_read), xc(lines_read)

      external rms
      external istderr

c     -- Assigning input variables to old TISEAN variables --
      if (rv.gt.0.) then
        eps = rv
        frac = 0.
      else
        eps = 0.
        frac = -rv
      endif
      nmax = lines_read

      call rms(nmax,in_out1,sc,sd)
      if(frac.gt.0) eps=sd*frac
      do 10 n=1,nmax
        x0(n)=in_out1(n)
 10     continue
      do 20 it=1,imax
         call nrlazy(nmax,in_out1,xc,m,eps)
c        -- when done with iterations write output --
         if(it.eq.imax) then
            goto 999
         endif

         eps=0.
         do 40 n=1,nmax
            eps=eps+(xc(n)-in_out1(n))**2
 40         in_out1(n)=xc(n)          
         eps=sqrt(eps/nmax)
         if(abs(eps).le.1e-312) then
            call xstopx ('ts_lazy: Zero correction, finished')
         endif

c     -- display information about diameter -- 
 20      if(iverb.eq.1) write(istderr(),*) 
     .      'ts_lazy: New diameter of neighbourhoods is ', eps

c     -- write the output --
 999  do 50 n=1,nmax
c     -- old code: write(iunit,*) xc(n), x0(n)-xc(n)--
        in_out1(n) = xc(n)
        in_out2(n) = x0(n) - xc(n)
 50     continue
      end
 

      subroutine nrlazy(nmax,y,yc,m,eps)
      implicit none
c     -- parameters --
      integer*4 im, nx
      parameter(im=100,nx=1000000) 
c     -- input variables --
      integer*4 nmax, m
      real*8 eps
c     -- input arrays-- 
      real*8 y(nmax),yc(nmax)
c     -- local variables --
      integer*4 n, nn, nfound
      real*8 av
c     -- local arrays --
      integer*4 jh(0:im*im),jpntr(nx),nlist(nx)

      if(nmax.gt.nx) call xstopx ("nrlazy: make nx larger")
      call base(nmax,y,1,m,jh,jpntr,eps)
      do 10 n=1,nmax
 10      yc(n)=y(n)   
      do 20 n=m,nmax           
         call neigh(nmax,y,y,n,nmax,1,m,jh,jpntr,eps,nlist,nfound)
         av=0.
         do 30 nn=1,nfound            
 30         av=av+y(nlist(nn)-(m-1)/2)              ! average middle coordinate
 20      yc(n-(m-1)/2)=av/nfound
      end
