c===========================================================================
c
c   This file is part of TISEAN
c 
c   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
c 
c   TISEAN is free software; you can redistribute it and/or modify
c   it under the terms of the GNU General Public License as published by
c   the Free Software Foundation; either version 2 of the License, or
c   (at your option) any later version.
c
c   TISEAN is distributed in the hope that it will be useful,
c   but WITHOUT ANY WARRANTY; without even the implied warranty of
c   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c   GNU General Public License for more details.
c
c   You should have received a copy of the GNU General Public License
c   along with TISEAN; if not, write to the Free Software
c   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
c
c===========================================================================
c   locate unstable periodic points
c   author T. Schreiber (1998)
c===========================================================================
c   modified Piotr Held (2015)
c===========================================================================
c==== NOTE MUST BE COMPILED WITH flag -freal-4-real-8 ======

c -- globalvars variables --
      module globalvars
         real*8, dimension (:), allocatable :: x(:)
         integer*4 nmax, m
         real*8 eps
      end module

      subroutine ts_upo(m_in, eps_in, frac,teq,tdis,h,
     $                  tacc,iper,icen,
     $                  lines_read, in_out1, olens, orbit_data, sizedat,
     $                  acc, stability);
      use globalvars
      implicit none
c -- input declarations --
      integer*4 m_in, iper, icen, lines_read, sizedat
      real*8 eps_in, frac, teq, tdis, h, tacc
c     the *_in is used because they need to be converted from double to float

      real*8 in_out1(lines_read), orbit_data(sizedat)
      real*8 acc(icen), stability(icen), olens(icen)

c -- parameters --
      integer*4 mper
      parameter(mper=20)

c -- variable declarations --

c     from main body
      integer*4 n
      integer istderr
      real*8 sc,sd
      external istderr, rms
c -- functions --
      integer known, isold
      integer*4 iperiod
      real stab

c     from old findupo() -- might cause problems
      integer*4 info, nfev, ndum, itry, ior
      integer*4 i, ipor
      integer*4 iw(mper)
      real tol
      real enorm, r1mach
      real xp(mper),fvec(mper)
      real, dimension (:,:), allocatable :: xor

      real w0(mper,mper),w1(mper),w2(mper),w3(mper)
      real w4(mper),w5(mper),w6(mper)
      real*8 err, sor
      external peri, enorm, r1mach, snls1

c     new for creating an output
      integer*4 orbit_no, data_pos

c     memory allocation flags
      integer*4 flag, AllocateStatus
      flag = 0

c     allocating memory
      allocate (xor(mper,lines_read), STAT = AllocateStatus)
      flag = flag + AllocateStatus
c -- Assign input variables to program variables --
      nmax=lines_read
      m=m_in
      eps=eps_in
      allocate (x(nmax), STAT = AllocateStatus)
      flag = flag + AllocateStatus

      if(flag.ne.0) then
         call xstopx ("ts_upo: not enough memory.")
      endif

      do 1 i=1,lines_read
         x(i)=in_out1(i)
 1       continue

c -- Prepare the data --
      call rms(nmax,x,sc,sd)
      if(frac.gt.0) eps=sd*frac
      if(teq.lt.0.) teq=eps
      if(tdis.lt.0.) tdis=eps
      if(tacc.lt.0.) tacc=eps
      if(h.lt.0.) h=eps

c -- Prepare iterators for output --
      orbit_no = 0;
      data_pos = 0;

c -- Exectute main program --
c     old call findupo(iper,icen,teq,tdis,tacc,h,iunit,iverb)
      if(iper.gt.mper) then
         call xstopx ("findupo: make mper larger.")
      endif
      tol=sqrt(r1mach(4))
      itry=0
      ior=0
      do 10 n=iper,nmax
c         if(iv_10(iverb).eq.1) then
c            if(mod(n,10).eq.0) write(istderr(),'(i7)') n
c         else if(iv_100(iverb).eq.1) then
c            if(mod(n,100).eq.0) write(istderr(),'(i7)') n
c         else if(iv_1000(iverb).eq.1) then
c            if(mod(n,1000).eq.0) write(istderr(),'(i7)') n
c         endif
         if(known(n,iper,teq).eq.1) goto 10
         itry=itry+1
         if(itry.gt.icen) goto 999
         do 20 i=1,iper
 20         xp(i)=x(n-iper+i)
         call snls1(peri,1,iper,iper,xp,fvec,w0,mper,tol,tol,0.,
     .      20*(iper+1),0.,w1,1,100.,0,info,nfev,ndum,iw,w2,w3,w4,w5,w6)
         err=enorm(iper,fvec)
         if(info.eq.-1.or.info.eq.5.or.err.gt.tacc) goto 10   ! unsuccessfull
         if(isold(iper,xp,ior,xor,tdis).eq.1) goto 10         ! already found
         ior=ior+1                                            ! a new orbit
         do 30 i=1,iper
 30         xor(i,ior)=xp(i)
         ipor=iperiod(iper,xp,tdis)
         sor=real(ipor)*stab(iper,xp,h)/real(iper)

c        old call print(iper,xp,ipor,sor,err,iunit,iverb)
         orbit_no = orbit_no+1
         olens(orbit_no+1) = ipor
         acc(orbit_no+1) = err
         stability(orbit_no+1) = exp(sor)
         do 40 i=1,ipor
            data_pos = data_pos + 1
            orbit_data(data_pos+1) = xp(i)
 40      continue
 10   continue

c -- write lengths input arrays --
 999  olens(1) = orbit_no
      orbit_data(1) = data_pos
      acc(1) = orbit_no
      stability(1) = orbit_no

      deallocate (x)
      deallocate (xor)

      end

      integer function known(n,iper,tol)
c return 1 if equivalent starting point has been tried
      use globalvars

      known=1
      do 10 nn=iper,n-1
         dis=0
         do 20 i=1,iper
 20         dis=dis+(x(n-iper+i)-x(nn-iper+i))**2
 10      if(sqrt(dis).lt.tol) return
      known=0
      end

      integer function isold(iper,xp,ior,xor,toler)
c determine if orbit is in data base
      parameter(mper=20)
      dimension xp(iper), xor(mper,*)

      isold=1
      do 10 ip=1,iper
         do 20 io=1,ior
            dor=0
            do 30 i=1,iper
 30            dor=dor+(xp(i)-xor(i,io))**2
 20            if(sqrt(dor).le.toler) return
 10      call oshift(iper,xp)
      isold=0
      end
  
      subroutine oshift(iper,xp)
c leftshift orbit circularly by one position
      dimension xp(*)

      h=xp(1)
      do 10 i=1,iper-1
 10      xp(i)=xp(i+1)
      xp(iper)=h
      end
 
      integer*4 function iperiod(iper,xp,tol)
c determine shortest subperiod
      dimension xp(*)

      do 10 iperiod=1,iper
         dis=0
         do 20 i=1,iper
            il=i-iperiod
            if(il.le.0) il=il+iper
 20         dis=dis+(xp(i)-xp(il))**2
 10      if(sqrt(dis).le.tol) return
      end

      subroutine peri(iflag,mf,iper,xp,fvec)
c built discrepancy vector (as called by snls1)
      dimension xp(iper),fvec(mf)

      do 10 ip=1,iper
         fvec(ip)=xp(1)-fc(iper,xp,iflag)
 10      call oshift(iper,xp)
      end

      function fc(iper,xp,iflag)
c predict (cyclic) point 1, using iper,iper-1...
      use globalvars
      dimension  xp(*)


      data cut/20/

      eps2=1./(2*eps*eps)
      ft=0
      sw=0
      fc=0
      do 10 n=m+1,nmax
         dis=0
         do 20 i=1,m
 20         dis=dis+(x(n-i)-xp(mod(m*iper-i,iper)+1))**2
         ddis=dis*eps2
         w=0
         if(ddis.lt.cut) then
            w=exp(-ddis)
         endif
         ft=ft+w*x(n)
 10      sw=sw+w
      iflag=-1
      if(abs(sw).le.1e-312) return   ! fc undefined, stop minimising
      fc=ft/sw
      iflag=1
      end

      real function stab(ilen,xp,h)
c compute cycle stability by iteration of a tiny perturbation
      use globalvars
      parameter(mper=20,maxit=1000)
      dimension xp(*), xcop(mper)


      if(mper.lt.ilen) then
         call xstopx ("stability: make mper larger.")
      endif
      iflag=1
      stab=0
      do 10 i=2,m
 10      xcop(i)=xp(mod(i-1,ilen)+1)
      xcop(1)=xp(1)+h
      do 20 it=1,maxit
         do 30 itt=1,ilen
            xx=fc(m,xcop,iflag)
            if(iflag.eq.-1) goto 1
            call oshift(m,xcop)
 30         xcop(m)=xx
         dis=0
         do 40 i=1,m
 40         dis=dis+(xcop(i)-xp(mod(i-1,ilen)+1))**2
         dis=sqrt(dis)
         stab=stab+log(dis/h)
         do 20 i=1,m
 20         xcop(i)=xp(mod(i-1,ilen)+1)*(1-h/dis) + xcop(i)*h/dis
 1    stab=stab/max(it-1,1)
      end
