c===========================================================================
c
c   This file is part of TISEAN
c 
c   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
c 
c   TISEAN is free software; you can redistribute it and/or modify
c   it under the terms of the GNU General Public License as published by
c   the Free Software Foundation; either version 2 of the License, or
c   (at your option) any later version.
c
c   TISEAN is distributed in the hope that it will be useful,
c   but WITHOUT ANY WARRANTY; without even the implied warranty of
c   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c   GNU General Public License for more details.
c
c   You should have received a copy of the GNU General Public License
c   along with TISEAN; if not, write to the Free Software
c   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
c
c===========================================================================
c   Create multivariate surrogate data
c   author T. Schreiber (1999)
c===========================================================================
c   modified Piotr Held (2015)
c===========================================================================
c===== NOTE MUST BE COMPILED WITH flag -freal-4-real-8 =====
      subroutine ts_surrogates (xx, nx, mx, imax, ispec, seed,
     $                          output, iterations, rel_discrepency);

      dimension xx(nx,mx), x(nx,mx), y(nx,mx), xamp(nx,mx)
      dimension xsort(nx,mx), list(nx), rwork(nx), output(nx,mx)
c     w1,w2 are used in store_spec      
      integer*4 flag, AllocateStatus
      real*8, dimension(:), allocatable :: w1,w2
      external rand

      r=rand(sqrt(abs(seed)))
c     -- mcmax - component number --
      mcmax = mx
c     -- nmaxp - max number of elements in each component --
      nmaxp = nx

c     allocating memory for w1, w2 used in store_spec
      flag = 0
      allocate(w1(nmaxp), STAT = AllocateStatus)
      flag = flag + AllocateStatus
      allocate(w2(nmaxp), STAT = AllocateStatus)
      flag = flag + AllocateStatus

      if(flag.ne.0) then
         call xstopx ("ts_surrogates: not enough memory.")
      endif

c     performing computatinos
      do 20 m=1,mcmax
         do 30 n=1,nmaxp
            x(n,m)=xx(n,m)
            y(n,m)=x(n,m)
            xamp(n,m)=x(n,m)
 30         xsort(n,m)=x(n,m)
         call store_spec(nmaxp,xamp(1,m),0,w1,w2)
         call sort(nmaxp,xsort(1,m),list)
         do 40 n=1,nmaxp
 40         rwork(n)=rand(0.0)
         call rank(nmaxp,rwork,list)
 20      call index2sort(nmaxp,x(1,m),list)
      it=-1
      dspec=r1mach(2)
 1    it=it+1
      do 50 m=1,mcmax
         do 50 n=1,nmaxp
 50         y(n,m)=x(n,m)
      ds0=dspec
      dspec=toxspec(nmaxp,mcmax,nx,xamp,y)
      if(imax.ge.0.and.it.ge.imax) goto 2
      do 60 m=1,mcmax
 60      call todist(nmaxp,xsort(1,m),y(1,m),x(1,m))
      if(dspec.lt.ds0) goto 1
 2    continue
      if(ispec.gt.0) then
c        assign output for when exact is set (ispec > 0)
         output = y
      else
c        assign output for when exact is not set (ispec == 0)
         output = x
      endif
      iterations = it
      rel_discrepency = dspec

c     deallocating w1, w2 the store_spec arrays
      deallocate(w2)
      deallocate(w1)

      end

      function toxspec(nmax,mmax,nxx,a,x)
      parameter(tol=1e-5)
      dimension x(nxx,mmax), a(nxx,mmax), iw(15)

      real*8, dimension (:), allocatable :: w1,w2,goal
      real*8, dimension (:,:), allocatable :: w

c     allocate memory
      integer*4 flag, AllocateStatus
      flag = 0
c     the following is a workaround due to compiler warnings
      allocate(w(nmax,mmax))
      if (allocated(w)) then
         AllocateStatus = 0
      else
         AllocateStatus = 1
      endif
      flag = flag + AllocateStatus
      allocate(w1(nmax), STAT = AllocateStatus)
      flag = flag + AllocateStatus
      allocate(w2(nmax), STAT = AllocateStatus)
      flag = flag + AllocateStatus
      allocate(goal(mmax), STAT = AllocateStatus)
      flag = flag + AllocateStatus

      if(flag.ne.0) then
         call xstopx ("toxspec: not enough memory.")
      endif
      call rffti1(nmax,w2,iw)  
      do 10 m=1,mmax
         do 20 n=1,nmax
 20         w(n,m)=x(n,m)
         call rfftf1(nmax,x(1,m),w1,w2,iw)
         do 30 n=1,nmax
 30         x(n,m)=x(n,m)/real(nmax)
         x(1,m)=sqrt(a(1,m))
         do 40 n=2,(nmax+1)/2
            pha=atan2(x(2*n-1,m),x(2*n-2,m))
            x(2*n-2,m)=sqrt(a(2*n-2,m))
 40         x(2*n-1,m)=pha
 10      if(mod(nmax,2).eq.0) x(nmax,m)=sqrt(a(nmax,m))
      if(mmax.gt.1) then
         do 50 n=2,(nmax+1)/2
            do 60 m=1,mmax
 60            goal(m)=x(2*n-1,m)-a(2*n-1,m)
            alpha=alp(mmax,goal)
            do 50 m=1,mmax
 50            x(2*n-1,m)=alpha+a(2*n-1,m)
      endif
      do 70 m=1,mmax
         do 80 n=2,(nmax+1)/2
            c=x(2*n-2,m)*cos(x(2*n-1,m))
            s=x(2*n-2,m)*sin(x(2*n-1,m))
            x(2*n-1,m)=s
 80         x(2*n-2,m)=c
 70      call rfftb1(nmax,x(1,m),w1,w2,iw)
      toxspec=0
      do 90 m=1,mmax
         do 90 n=1,nmax
 90         toxspec=toxspec+(x(n,m)-w(n,m))**2
      toxspec=sqrt((toxspec/nmax)/mmax)

      deallocate(goal)
      deallocate(w2)
      deallocate(w1)
      deallocate(w)

      end

      function alp(mmax,goal)
      dimension goal(mmax)
      data pi/3.1415926/

      f1=0
      f2=0
      do 10 m=1,mmax
         f1=f1+cos(goal(m))
 10      f2=f2+sin(goal(m))
      alp=atan2(f2,f1)
      scos=0
      do 20 m=1,mmax
 20      scos=scos+cos(alp-goal(m))
      if(scos.lt.0) alp=alp+pi
      end

      subroutine todist(nmax,dist,x,y)
      dimension x(nmax), dist(nmax), y(nmax)
      integer*4, dimension (:), allocatable :: list
      integer*4 flag

      allocate(list(nmax), STAT = flag)

      if(flag.ne.0) then
         call xstopx ("ts_surrogates:todist: not enough memory.")
      endif

      call rank(nmax,x,list)
      do 10 n=1,nmax
 10      y(n)=dist(list(n))

      deallocate(list)

      end

