/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger
 * Modified: Piotr Held <pjheld@gmail.com> (2015). 
 * This function is based on exclude_interval of TISEAN 3.0.1 https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/

#include <octave/oct.h>

octave_idx_type exclude_interval(octave_idx_type n,long ex0,long ex1,
                               unsigned long *hf,octave_idx_type *found)
{
  long help;
  octave_idx_type lf=0;
  
  for (octave_idx_type i=0;i<n;i++) 
    {
      help=hf[i];
      if ((help < ex0) || (help > ex1))
        found[lf++]=help;
    }
  return lf;
}
