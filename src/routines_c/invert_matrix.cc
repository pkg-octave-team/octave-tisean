/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger Last modified: Sep 5, 2004*/
/* Changes: 
 * Sep 5, 2004: added the extern check_alloc line
 */
#include <octave/oct.h>

extern void check_alloc(void*);

void invert_matrix(double **mat,double **out_imat, octave_idx_type size)
{
  extern void solvele(double**,double*,octave_idx_type);

  OCTAVE_LOCAL_BUFFER (double, hmat_data, size * size);
  OCTAVE_LOCAL_BUFFER (double *, hmat, size);
  for (octave_idx_type i=0;i<size;i++) {
    hmat[i]=hmat_data + size * i;
  }

  OCTAVE_LOCAL_BUFFER (double, vec, size);
  for (octave_idx_type i=0;i<size;i++) {
    for (octave_idx_type j=0;j<size;j++) {
      vec[j]=(i==j)?1.0:0.0;
      for (octave_idx_type k=0;k<size;k++)
      hmat[j][k]=mat[j][k];
    }
    solvele(hmat,vec,size);
    for (octave_idx_type j=0;j<size;j++)
      out_imat[j][i]=vec[j];
  }
}
