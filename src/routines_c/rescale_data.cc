/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger.
 * Modified: Piotr Held <pjheld@gmail.com> (2015). 
 * This function is based on rescale_data of TISEAN 3.0.1 https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/

#include <octave/oct.h>

//! This function is for internal use only
//! Provides functionalit for rescale_data()
void __rescale_data__ (double *x, octave_idx_type l, double *min,
                       double *interval)
{
  *min=*interval=x[0];
  
  for (octave_idx_type i=1;i<l;i++) {
    if (x[i] < *min) *min=x[i];
    if (x[i] > *interval) *interval=x[i];
  }
  *interval -= *min;

  if (*interval != 0.0) {
    for (octave_idx_type i=0;i<l;i++)
      x[i]=(x[i]- *min)/ *interval;
  }
  else {
    error_with_id ("Octave:invalid-input-arg", \
          "rescale_data: data ranges from %e to %e. It makes"
          " no sense to continue",*min,*min+(*interval));
  }
}

void rescale_data(Matrix &x,octave_idx_type column,octave_idx_type l,double *min,double *interval)
{
  __rescale_data__ (x.fortran_vec () + column * l, l, min, interval);
}

void rescale_data(NDArray &x,octave_idx_type l,double *min,double *interval)
{
  __rescale_data__ (x.fortran_vec (), l, min, interval);
}
