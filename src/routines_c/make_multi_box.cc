/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger
 * Modified: Piotr Held <pjheld@gmail.com> (2015). 
 * This function is based on make_multi_box of TISEAN 3.0.1 https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/


#include <octave/oct.h>

void make_multi_box(const Matrix &ser, MArray<octave_idx_type> &box,
                    long *list,octave_idx_type l, octave_idx_type bs,
                    octave_idx_type dim,octave_idx_type emb,
                    octave_idx_type del,double eps)
{
  octave_idx_type ib=bs-1;

  for (octave_idx_type x=0;x<bs;x++)
    for (octave_idx_type y=0;y<bs;y++)
      box(y,x) = -1;

  octave_idx_type x,y;
  for (octave_idx_type i=(emb-1)*del;i<l;i++) 
    {
      x=(octave_idx_type)(ser(i,0)/eps)&ib;
      y=(octave_idx_type)(ser(i,dim-1)/eps)&ib;
      list[i]=box(y,x);
      box(y,x)=i;
    }
}

void make_multi_box(const double **ser, octave_idx_type **box,
                    long *list,octave_idx_type l, octave_idx_type bs,
                    octave_idx_type dim,octave_idx_type emb,
                    octave_idx_type del,double eps)
{
  octave_idx_type ib=bs-1;

  for (octave_idx_type x=0;x<bs;x++)
    for (octave_idx_type y=0;y<bs;y++)
      box[x][y] = -1;

  octave_idx_type x,y;
  for (octave_idx_type i=(emb-1)*del;i<l;i++) 
    {
      x=(octave_idx_type)(ser[0][i]/eps)&ib;
      y=(octave_idx_type)(ser[dim-1][i]/eps)&ib;
      list[i]=box[x][y];
      box[x][y]=i;
    }
}

