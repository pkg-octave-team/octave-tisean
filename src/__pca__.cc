/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger */
/* Modified: Piotr Held <pjheld@gmail.com> (2015). 
 * This function is based on pca of TISEAN 3.0.1
 * https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>

#include "routines_c/tsa.h"

void ordne(double *lyap,int *ord,octave_idx_type dimemb)
{
  long i,j,maxi;
  double max;
  
  for (i=0;i<dimemb;i++)
    ord[i]=i;

  for (i=0;i<dimemb-1;i++)
    for (j=i+1;j<dimemb;j++)
      if (lyap[i] < lyap[j]) {
        max=lyap[i];
        lyap[i]=lyap[j];
        lyap[j]=max;
        maxi=ord[i];
        ord[i]=ord[j];
        ord[j]=maxi;
      }
}

DEFUN_DLD (__pca__, args, , HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 7)
    {
      print_usage();
    }
  else
    {

      Matrix input          = args(0).matrix_value ();
      octave_idx_type DIM   = args(1).idx_type_value ();
      octave_idx_type EMB   = args(2).idx_type_value ();
      octave_idx_type DELAY = args(3).idx_type_value ();
      octave_idx_type LDIM  = args(4).idx_type_value ();
      bool projection_set   = args(5).bool_value ();
      int w                 = args(6).int_value ();

      octave_idx_type LENGTH = input.rows ();

      bool write_values=0,write_vectors=0;
      bool write_comp=0,write_proj=0;

      write_values = 1;
      if (w > 0)
        write_vectors = 1;
      if (w == 2)
        write_comp = 1;
      if (w == 3)
        write_proj = 1;

      octave_idx_type dimemb=DIM*EMB;
      if (!projection_set)
        LDIM=dimemb;
      else {
        if (LDIM < 1) LDIM=1;
        if (LDIM > dimemb) LDIM=dimemb;
      }

      OCTAVE_LOCAL_BUFFER (double, av, DIM);
      double rms;

      for (octave_idx_type j=0;j<DIM;j++) {
        av[j]=rms=0.0;
        variance(input.column(j),LENGTH,&av[j],&rms);
        for (octave_idx_type i=0;i<LENGTH;i++)
          input(i,j) -= av[j];
      }
      
      // old make_pca()
      octave_idx_type i1,i2,j1,j2,k1,k2;
      double hsp=0.0;

      OCTAVE_LOCAL_BUFFER (int, ord, dimemb);
      OCTAVE_LOCAL_BUFFER (double, eig, dimemb);
      OCTAVE_LOCAL_BUFFER (double, matarray, dimemb*dimemb);
      OCTAVE_LOCAL_BUFFER (double*, mat, dimemb);

      for (octave_idx_type i=0;i<dimemb;i++)
        mat[i]=(double*)(matarray+i*dimemb);

      for (octave_idx_type i=0;i<dimemb;i++) {
        i1=i/EMB;
        i2=(i%EMB)*DELAY;
        for (octave_idx_type j=i;j<dimemb;j++) {
          j1=j/EMB;
          j2=(j%EMB)*DELAY;
          mat[i][j]=0.0;
          for (octave_idx_type k=(EMB-1)*DELAY;k<LENGTH;k++)
            mat[i][j] += input(k-i2,i1)*input(k-j2,j1);
          mat[j][i]=(mat[i][j] /= (double)(LENGTH-(EMB-1)*DELAY));
        }
      }

      eigen(mat,dimemb,eig);
      ordne(eig,ord,dimemb);

      Matrix eigvals (dimemb, 2);

      int rows = 0;
      int cols = 0;

      if (write_vectors)
        {
          rows = dimemb;
          cols = dimemb;
        }

      Matrix eigvec  (rows, cols);

      rows = 0;
      cols = 0;

      if (write_comp)
        {
          rows = LENGTH-(EMB-1)*DELAY;
          cols = LDIM;
        }

      Matrix retcomp (rows, cols);

      rows = 0;
      cols = 0;

      if (write_proj)
        {
          rows = LENGTH;
          cols = DIM;
        }

      Matrix retproj (rows, cols);

      for (octave_idx_type i=0;i<dimemb;i++)
        if (write_values) {
    //    fprintf(stdout,"%d %e\n",i,eig[i]);
          eigvals(i,0) = i;
          eigvals(i,1) = eig[i];
        }

      if (write_vectors) {
        for (octave_idx_type i=0;i<dimemb;i++) {
          for (octave_idx_type j=0;j<dimemb;j++) {
            j1=ord[j];
      //    fprintf(fout,"%e ",mat[i][j1]);
            eigvec (i,j) = mat[i][j1];
          }
        }
      }

      if (write_comp) {
        for (octave_idx_type i=(EMB-1)*DELAY;i<LENGTH;i++) {
          for (octave_idx_type j=0;j<LDIM;j++) {
            j1=ord[j];
            hsp=0.0;
            for (octave_idx_type k=0;k<dimemb;k++) {
              k1=k/EMB;
              k2=(k%EMB)*DELAY;
              hsp += mat[k][j1]*(input(i-k2,k1)+av[k1]);
            }
    //      fprintf(fout,"%e ",hsp);
            retcomp(i-(EMB-1)*DELAY,j) = hsp;
          }
        }
      }

      if (write_proj) 
        {
          OCTAVE_LOCAL_BUFFER (double, sp, LDIM);

          for (octave_idx_type i=0;i<(EMB-1)*DELAY;i++) 
            {
              for (octave_idx_type j=0;j<DIM;j++)
        ////    fprintf(stdout,"%e ",series[j][i]+av[j]);
                retproj(i,j) = input(i,j)+av[j];
            }
          for (octave_idx_type i=(EMB-1)*DELAY;i<LENGTH;i++) 
            {
              for (octave_idx_type j=0;j<LDIM;j++) 
                {
                  j1=ord[j];
                  sp[j]=0.0;
                  for (octave_idx_type k=0;k<dimemb;k++) 
                    {
                      k1=k/EMB;
                      k2=(k%EMB)*DELAY;
                      sp[j] += mat[k][j1]*input(i-k2,k1);
                    }
                }
              for (octave_idx_type j=0;j<DIM;j++) 
                {
                  hsp=0.0;
                  for (octave_idx_type k=0;k<LDIM;k++) 
                    {
                      k1=ord[k];
                      hsp += mat[j*EMB][k1]*sp[k];
                    }
    ////    fprintf(stdout,"%e ",hsp+av[j]);
                 retproj(i,j) = hsp + av[j];
               }
            }
        }
      

      retval(0) = eigvals;
      retval(1) = eigvec;
      if (write_comp)
        retval(2) = retcomp;
      if (write_proj)
        retval(2) = retproj;
    }

  return retval;
}
