TISEAN Package port for Octave
====

TISEAN stands for TIme SEries ANalysis.

More information can be found on the TISEAN website:

[http://www.mpipks-dresden.mpg.de/~tisean/Tisean_3.0.1/](http://www.mpipks-dresden.mpg.de/~tisean/Tisean_3.0.1/)

And on the Octave Package Wiki page:

[http://wiki.octave.org/TISEAN_package](http://wiki.octave.org/TISEAN_package)


To use the functions use the m-files. The `__*__.oct` files are for internal use.

The m-files are in folder `./inst`

Before compilation go to root folder and execute: 

1. `./bootstrap`
2. `cd src && ./configure && cd ..`

Compile using:

`make all`

If you want to check out how these functions work use:

`make run`