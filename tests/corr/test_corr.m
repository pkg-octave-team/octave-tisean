# This test compares the results of TISEAN corr function
# $ corr square.dat -D6000 -n -o "square_tisean.dat"
# $ corr square.dat -D6000 -o "square_tisean_norm.dat"
# $ corr amplitude.dat -D5000 -n -o "amplitude_tisean.dat"
# $ corr amplitude.dat -D5000 -o "amplitude_tisean_norm.dat"
# $ corr ar.dat -D5000 -n -o "ar_tisean.dat"
# $ corr ar.dat -D5000 -o "ar_tisean_norm.dat"

pkg load signal
close all

dataset = {"square","amplitude", "ar"};
ndata   = numel (dataset);

tisean_output = @(x) sprintf("%s_tisean.dat",x);
tisean_norm_output = @(x) sprintf("%s_tisean_norm.dat",x);

for i=1:ndata

  # Load tisean results
  data_tisean = load (tisean_output (dataset{i}));
  data_tisean_norm = load (tisean_norm_output (dataset{i}));
  
  data = load ([dataset{i} ".dat"]);
  n    = length (data);

  # Calculate with Octave  
  [data_octave, lags] = xcorr (data, n,'unbiased');
  idx         = find (lags >0);
  data_octave = data_octave(idx);
  lags        = lags(idx);

  #Calculate with Octave when normalised
  data_octave_norm = xcorr (center (data), n, 'coeff');
  data_octave_norm = data_octave_norm(idx);
  data_octave_norm = data_octave_norm .* (n ./ (n - (transpose ([0:n-1]))));  

  # Compare
  figure (2*i - 1)
  plot (lags, data_octave,'r.', ...
        data_tisean(:,1), data_tisean(:,2),'bo')
  legend ("Octave","Tisean");
  axis tight

  figure (2*i)
  plot (lags, data_octave_norm,'r.', ...
        data_tisean_norm(:,1), data_tisean_norm(:,2),'bo')
  legend ("Octave","Tisean");
  axis tight

  printf ("Difference on %s: %.3g\n", dataset{i}, ...
                   sqrt (mean ((data_octave-data_tisean(:,2)).^2)));
  printf ("Difference on %s when normalized: %.3g\n", dataset{i}, ...
                   sqrt (mean ((data_octave_norm-data_tisean_norm(:,2)).^2)));
  
  fflush (stdout);

endfor
