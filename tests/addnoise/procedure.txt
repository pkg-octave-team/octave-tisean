As 'addnoise' simply adds noise to the input vector, it can be easily replaced with GNU Octave functions.

So to replace the following command in Octave:
    $ addnoise data.dat -v0.02 -u -o "data_noise.dat"

    r = std (data) * 0.02;
    data = data + r * rand (size (data))


To replace the following command in Octave:
    $ addnoise data.dat -r0.5 -o "data_noise.dat"

    g = (-6 + sum (rand ([size(data), 12]), 3));    
    data = data + 0.5 * g
    
