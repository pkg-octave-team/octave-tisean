# This test compares the results of TISEAN histogram function
# $ histogram amplitude.dat -o "amplitude_tisean.dat"

pkg load signal
close all

dataset = {"amplitude"};
ndata   = numel (dataset);

tisean_output = @(x) sprintf("%s_histogram.dat",x);


for i=1:ndata

  # Load tisean results
  data_tisean = load (tisean_output (dataset{i}));
  
  data = load ([dataset{i} ".dat"]);
  n    = length (data);

  # Calculate with Octave  
  [no_octave, centers_octave] = hist (data, 50, 1);
  no_octave = transpose (no_octave);
  centers_octave = transpose (centers_octave);
  
  # Compare
  figure (i)
  h = bar (centers_octave, [no_octave,data_tisean(:,2)]);
  set (h(1), "facecolor", "b");
  set (h(2), "facecolor", "g");  
  legend ("Octave","Tisean");

  axis tight


  printf ("Difference on %s: %.3g\n", dataset{i}, ...
                   sqrt (mean ((no_octave-data_tisean(:,2)).^2)));

  fflush (stdout);

endfor
