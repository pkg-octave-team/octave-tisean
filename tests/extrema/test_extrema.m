# This test compares the results of TISEAN extrema function
# $ extrema amplitude.dat -o "amplitude_tisean_max.dat"
# $ extrema amplitude.dat -z -o "amplitude_tisean_min.dat"


pkg load signal
close all

dataset = {"amplitude"};
ndata   = numel (dataset);

tisean_max_output = @(x) sprintf("%s_tisean_max.dat",x);
tisean_min_output = @(x) sprintf("%s_tisean_min.dat",x);

for i=1:ndata

  # Load tisean results
  data_tisean_max = load (tisean_max_output (dataset{i}));
  data_tisean_min = load (tisean_min_output (dataset{i}));
  
  data = load ([dataset{i} ".dat"]);
  n    = length (data);

  # Change to absolute time reference
  max_rows = rows (data_tisean_max);
  for j=1:max_rows
    data_tisean_max(max_rows+1-j,2) = sum (data_tisean_max([1:max_rows+1-j],2));
  endfor

  min_rows = rows (data_tisean_min);
  for j=1:min_rows
    data_tisean_min(min_rows+1-j,2) = sum (data_tisean_min([1:min_rows+1-j],2));
  endfor

  # Calculate with Octave  
  [pks_octave, loc, extra] = findpeaks (data, "DoubleSided", "MinPeakDistance", 2);
 
  # Compare
  figure (2*i - 1)
  plot ([1:5000],data,'b', ... 
          loc, pks_octave,'ro', ...
          data_tisean_max(:,2), data_tisean_max(:,1), 'g.');
  legend ("Data", "Octave","Tisean");
  axis tight

  figure (2*i)
  plot ([1:5000],data,'b', ... 
          loc, pks_octave,'ro', ...
          data_tisean_min(:,2), data_tisean_min(:,1), 'g.');
  legend ("Data", "Octave","Tisean");
  axis tight


#  printf ("Difference on %s: %.3g\n", dataset{i}, ...
#                   sqrt (mean ((data_octave-data_tisean(:,2)).^2)));
  
  fflush (stdout);

endfor
